package org.hung.oauth2.config;

import java.util.Arrays;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.security.oauth2.common.AuthenticationScheme;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;

@Configuration
public class OAuth2ClientConfig {

	@EnableWebSecurity
	public static class WebSecurityConfig extends WebSecurityConfigurerAdapter {

		@Override
		public void configure(WebSecurity web) throws Exception {
			web.debug(true);
		}

		@Override
		protected void configure(HttpSecurity http) throws Exception {
			// @formatter:off
			http
			  .authorizeRequests()
			  	.anyRequest().authenticated()
			.and()
			  .oauth2Login()
			    .userInfoEndpoint()
			      .customUserType(MyOAuth2User.class,"myclient1");
			// @formatter:on
		}
		
	}
	
	@Configuration
	@EnableOAuth2Client
	public static class OAuthRemoteResConfig {
		
	    @Bean
	    public OAuth2ProtectedResourceDetails oAuth2ProtectedResourceDetails() {
	        AuthorizationCodeResourceDetails details = new AuthorizationCodeResourceDetails();
	        details.setId("dummy");	        
	        details.setClientId("client1");
	        details.setClientSecret("password");
	        details.setGrantType("authorization_code");
	        details.setClientAuthenticationScheme(AuthenticationScheme.header);
	        details.setUserAuthorizationUri("http://localhost:7070/oauth/authorize");
	        details.setAccessTokenUri("http://localhost:7070/oauth/token");	        
	        details.setScope(Arrays.asList("read"));
	        return details;
	    }
	 
	    @Bean
	    public OAuth2RestTemplate createRestTemplate(OAuth2ProtectedResourceDetails details, OAuth2ClientContext clientContext) {
	        return new OAuth2RestTemplate(details, clientContext);
	    }	
		
	}
}
