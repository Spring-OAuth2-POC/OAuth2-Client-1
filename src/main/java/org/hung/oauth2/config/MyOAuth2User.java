package org.hung.oauth2.config;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.core.user.OAuth2User;

public class MyOAuth2User implements OAuth2User, Serializable {
	
	private Map<String,Object> attributes;
	private List<GrantedAuthority> authorities;
	
	private String dn;
	private String username;
	private String displayName;
	private String department;
	private String mail;
	
	//public MyOAuth2User() {
	//	String aaa = this.name;
	//}
	
	@Override
	public String getName() {
		return getUsername();
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.authorities;
	}
	
	@Override
	public Map<String, Object> getAttributes() {
		if (this.attributes == null) {
			this.attributes = new HashMap<>();
			this.attributes.put("name", this.getName());
			this.attributes.put("displayName", this.getDisplayName());
			this.attributes.put("department", this.getDepartment());
			this.attributes.put("mail", this.getMail());
		}
		return attributes;
	}

	public String getDn() {
		return dn;
	}
	public void setDn(String dn) {
		this.dn = dn;
	}

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}

	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}

}
