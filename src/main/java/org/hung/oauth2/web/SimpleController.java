package org.hung.oauth2.web;

import org.hung.oauth2.config.MyOAuth2User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class SimpleController {
	
	@Autowired
	private OAuth2RestTemplate template;
	
	@GetMapping("/greeting")
	public String greeting(Model model) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		MyOAuth2User principal = (MyOAuth2User)authentication.getPrincipal();
		log.info("here!");
		
		String result = template.getForObject("http://localhost:6060/api/echo/345",String.class);
		log.info(result);
		return "greeting";
	}

}
